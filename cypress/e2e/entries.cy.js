/// <reference types="cypress" />
import entryContract from '../contracts/entry.contract'

describe('Entries endpoint testing', () => {

  it('Should validate all objects with Category equals to Authentication & Authorization', () => {
    cy.request({
        method: 'GET',
        url: 'entries',
        failOnStatusCode: false
    }).then((response) => {
        // Validating response status code
        expect(response.status).to.be.equal(200)

        // Finding objects with property category equals to Authentication & Authorization
        let authenticationAndAuthorizationEntries = response.body.entries.filter((entry) => entry.Category === 'Authentication & Authorization')

        // Variable to store the number of entries with category equals to Authentication & Authorization
        let numberOfAuthEntries = 0;

        authenticationAndAuthorizationEntries.forEach(entry => {
            // Counting the number of entries with category equals to Authentication & Authorization
            numberOfAuthEntries++

            // Validating that each object has all properties
            expect(entry).to.have.property('API')
            expect(entry).to.have.property('Description')
            expect(entry).to.have.property('Auth')
            expect(entry).to.have.property('HTTPS')
            expect(entry).to.have.property('Cors')
            expect(entry).to.have.property('Link')
            expect(entry).to.have.property('Category')

            // Validating the schema of each object
            entryContract.validateAsync(entry)

            // Printing the objects found
            cy.log(JSON.stringify(entry))
        })

        expect(numberOfAuthEntries).to.be.equal(7)
    })
  });

})