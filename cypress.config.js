const { defineConfig } = require("cypress");

module.exports = defineConfig({
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    reportDir: 'reports',
    charts: true,
    overwrite: true,
    embeddedScreenshots: true,
    inlineAssets: true,
    reportFilename: "[name]-report_[datetime]_[status]",
    timestamp: "mmmm-dd-yyyy-HH-MM-ss"
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      require('cypress-mochawesome-reporter/plugin')(on);
    },
    baseUrl: 'https://api.publicapis.org/',
  },
});
